# Python Week Of Code, Namibia 2019

[urls.py](urls.py) has the source code of a function
that uses the `re` library to match a URL provided by the user.

## Task for Instructor

1. Create a file [test.py](test.py) with

   ```
   import unittest

   from urls import post

   class TestUrls(unittest.TestCase):

       def test_blog_post_title(self):
           self.assertTrue(
               post('/blog/welcome/')
           )

   ```
2. Run

   ```
   python -m unittest test
   ```
3. Add

   ```
       def test_blog_post_id(self):
           self.assertTrue(
               post('/blog/1/')
           )
   ```

   to [test.py](test.py) and run

   ```
   python -m unittest test
   ```
4. Add

   ```
       def test_blog_post_empty(self):
           self.assertFalse(
               post('/blog//')
           )
   ```

   to [test.py](test.py) and run

   ```
   python -m unittest test
   ```

## Assert Methods

Some of the most commonly used methods:

- `assertEqual(a, b)`
- `assertNotEqual(a, b)`
- `assertTrue(x)`
- `assertFalse(x)`
- `assertIsNone(x)`
- `assertIsNotNone(x)`
- `assertIn(a, b)`
- `assertNotIn(a, b)`
- `assertRaises(exc, fun, *args, **kwds)`

The full list is available at https://docs.python.org/3/library/unittest.html#test-cases.

## Tasks for Learners

1. Fix the function `post()` in [urls.py](urls.py) to pass the test.
2. Add another test to [test.py](test.py).
